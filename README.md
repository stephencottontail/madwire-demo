# Madwire Demo #

This is a demo site, based on an actual WordPress photoblogging theme I'm currently designing.

## Features ##

* A custom widget which displays posts with the 'image' or 'gallery' format, selected by the user.
* An offcanvas menu.
* A full-screen background image, which can either be chosen by the user or set automatically based on the page being displayed.